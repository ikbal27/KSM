
<!-- PRELOADER Ends -->
<!-- Main Header including Banner Starts -->
<header class="main-header">
	<nav class="navbar navbar-fixed-top " data-spy="affix" data-offset-top="0" >
		<div class="container" style="padding: 0">
			<!-- secondary menu icon -->
			<!--/end secondary menu icon -->
			<div class="navbar-header">
				<!-- mobile menu icon -->
				<a href="#" id="nav-trigger" class="pull-right nav-trigger">
					<i class="icofont icofont-justify-all"></i>
				</a>
				<!--/end mobile menu icon -->
				<!-- site logo -->
				<div class="pull-left logo">
					<a href="<?=base_url()?>"><img src="<?=base_url().'assets/img/logo/logo_header.png'?>" alt="App Landing Page"></a>
				</div>
				<!--/end site logo -->
			</div>
			<div class="collapse navbar-collapse" id="navigation">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="<?=base_url()?>#home">Home</a></li>
					<li><a href="<?=base_url()?>#feature">Why Us</a></li>
					<li><a href="<?=base_url()?>#overview">Overview</a></li>
					<li><a href="<?=base_url()?>#review">Who's Us </a></li>
					<li><a href="<?=base_url()?>#feedback">Testimonials</a></li>
					<li><a href="<?=base_url()?>#driver">Drivers</a></li>
					<li><a href="blog.html">Orders</a>
						<ul>
							<li><a href="<?=base_url().'order/payment'?>">Upload Transaction</a></li>
						</ul>
					</li>
					<li><a href="<?=base_url()?>#contact">Contact</a></li>
				</ul>
			</div><!--/end navbar-collapse -->
		</div><!--/end container -->
	</nav>