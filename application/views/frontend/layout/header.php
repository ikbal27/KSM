
<!-- PRELOADER Ends -->
<!-- Secondary menu -->
<div class="secondary-menu-area">
	<ul class="boxshaodow-3 secondary-menu">
		<!-- if no menu added then primary menu will be shown automatically -->
	</ul>
	<i class="icofont icofont-ui-close close-icon"></i>
</div>
<!-- /Secondary menu -->
<!-- Main Header including Banner Starts -->
<header class="main-header">
	<nav class="navbar navbar-fixed-top transition" data-spy="affix" data-offset-top="100">
		<div class="container">
			<!-- secondary menu icon -->
			<a href="#" id="nav-trigger-2" class="pull-right hidden-xs secondary-menu-icon">
				<i class="icofont icofont-justify-all"></i>
			</a>
			<!--/end secondary menu icon -->
			<div class="navbar-header">
				<!-- mobile menu icon -->
				<a href="#" id="nav-trigger" class="pull-right nav-trigger">
					<i class="icofont icofont-justify-all"></i>
				</a>
				<!--/end mobile menu icon -->
				<!-- site logo -->
				<div class="pull-left logo">
					<a href="<?=base_url()?>"><img src="<?=base_url().'assets/img/logo/logo_header.png'?>" alt="App Landing Page"></a>
				</div>
				<!--/end site logo -->
			</div>
			<div class="collapse navbar-collapse" id="navigation">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="smooth" href="#home">Home</a></li>
					<li><a class="smooth" href="#feature">Why Us</a></li>
					<li><a class="smooth" href="#overview">Overview</a></li>
					<li><a class="smooth" href="#review">Who's Us </a></li>
					<li><a class="smooth" href="#feedback">Testimonials</a></li>
					<li><a class="smooth" href="#driver">Drivers</a></li>
					<li><a href="#">Orders</a>
						<ul>
							<li><a href="<?=base_url().'order/payment'?>">Upload Transaction</a></li>
						</ul>
					</li>
					<li><a class="smooth" href="#contact">Contact</a></li>
				</ul>
			</div><!--/end navbar-collapse -->
		</div><!--/end container -->
	</nav>