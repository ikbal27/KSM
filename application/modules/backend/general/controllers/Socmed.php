<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socmed extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['data'] = $this->model->get('socmed');
		$data['content'] = 'socmed_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function add($id='')
	{

		$data['content'] 		= 'socmed_add';
		$data['data'] 		  	= $this->model->get_where('socmed',array('socmedId'=> $id));
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save()
	{
		$post = $this->input->post();
		if (@$post['socmedId']) {
			if(hak_akses('update') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['updateDate'] = date('Y-m-d H:i:s');
			$post['updateBy'] = $this->session->userdata('usernameUser');
			$this->model->update_data('socmed',$post,array('socmedId'=>$post['socmedId']));
		} else {
			if(hak_akses('create') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['createDate'] = date('Y-m-d H:i:s');
			$post['createBy'] = $this->session->userdata('usernameUser');
			$this->model->insert_data('socmed',$post);
		}
		redirect(getModule().'/'.getController());
	}

	public function delete($id="")
	{
		$this->model->delete_data('socmed', 'socmedId='.$id);
	}

}

/* End of file socmed.php */
/* Location: ./application/modules/setting/controllers/socmed.php */