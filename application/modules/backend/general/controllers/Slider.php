<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['data'] = $this->model->get('slider');
		$data['content'] = 'slider_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function add($id='',$param='')
	{

		if ($param!="" && $param == "delete") {
			$this->load->library('uploads');
			$getLogo = $this->model->get_where('slider',array('sliderId'=>$id));
			
			$photo = $this->uploads->upload_image('sliderImage','assets/uploads/slider');
			unlink('assets/uploads/slider/'.$getLogo[0]['sliderImage']);
			unlink('assets/uploads/slider/thumb/'.$getLogo[0]['sliderImage']);
			
			$post['sliderImage'] = $photo['file_name'];
			$this->model->update_data('slider',$post,array('sliderId'=>$id));
			redirect(base_url(getModule().'/'.getController().'/add/'.$id),'refresh');
		}

		$data['content'] 		= 'slider_add';
		$data['data'] 		  	= $this->model->get_where('slider',array('sliderId'=> $id));
		$this->load->view('backend/main',$data,FALSE);
	}

	public function save()
	{
		$post = $this->input->post();

		if (@$_FILES['sliderImage']['name']!='') 
		{
			$this->load->library('uploads');
			$dir = "assets/uploads/slider/";
			$data = $this->uploads->upload_image('sliderImage',$dir);
			$name = $data['file_name'];
			$this->uploads->resize_image($dir.$name,$dir.'thumb/','525','350',''.$name);
			if(@$post['sliderImage']=="")
			{
				$post['sliderImage'] = $name; 
			}

		}

		if (@$post['sliderId']) {
			if(hak_akses('update') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['updateDate'] = date('Y-m-d H:i:s');
			$post['updateBy'] = $this->session->userdata('usernameUser');
			$this->model->update_data('slider',$post,array('sliderId'=>$post['sliderId']));
		} else {
			if(hak_akses('create') === FALSE){
				show_errPrivilege();
				exit();
			}
			$post['createDate'] = date('Y-m-d H:i:s');
			$post['createBy'] = $this->session->userdata('usernameUser');
			$this->model->insert_data('slider',$post);
		}
		redirect(getModule().'/'.getController());
	}

	public function delete($id="")
	{
		$getLogo = $this->model->get_where('slider',array('sliderId'=>$id));
		$this->model->delete_data('slider', 'sliderId='.$id);
		unlink('assets/uploads/slider/'.$getLogo[0]['sliderImage']);
		unlink('assets/uploads/slider/thumb/'.$getLogo[0]['sliderImage']);
	}

}

/* End of file Slider.php */
/* Location: ./application/modules/setting/controllers/Slider.php */