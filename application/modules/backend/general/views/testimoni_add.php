
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Testimoni </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save">
								<input type="hidden" name="testimoniId" class="form-control" value="<?php echo ($data) ? $data[0]['testimoniId'] : "" ?>">
								<?php echo input_text_group('testimoniUserName','Nama',(@$data[0]['testimoniUserName']) ? @$data[0]['testimoniUserName'] : set_value('testimoniUserName'),'Nama User ','') ?>
								<?php echo input_file_image_group('testimoniUserPicture','Foto Userr',!empty(@$data[0]['testimoniUserPicture']) ? base_url().'assets/uploads/testimoni/thumb/'. @$data[0]['testimoniUserPicture'] : '' ,   array('data-title' => 'Hapus gambar','data-desc' => 'Apakah anda yakin ingin menghapus gambar ini?','data-confirm' => 'Berhasil di hapus','data-route' => base_url(getModule()."/".getController()."/add/".@$data[0]['testimoniId']."/delete")), '')?>				
								<?php echo input_textarea_group('testimoniValue','Testimoni',(@$data[0]['testimoniValue']) ? @$data[0]['testimoniValue'] : set_value('testimoniValue'),'Testimoni','') ?>	
								<?php echo input_text_group('testimoniStar','Testimoni',(@$data[0]['testimoniStar']) ? @$data[0]['testimoniStar'] : set_value('testimoniStar'),'Star','', array('maxlength'=> '1')) ?>													
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>