
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel  panel-border panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Review </h3>
				</div>
				<div class="panel-body">
					<div class="row"> 
						<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>/save/review">

								<?php echo input_text_group('reviewVideo','Video',(@$data[0]['reviewVideo']) ? @$data[0]['reviewVideo'] :  '' ,' Video Video Review','') ?>	
								<?php echo input_text_group('reviewCaption','Caption',(@$data[0]['reviewCaption']) ? @$data[0]['reviewCaption'] :  '' ,' Caption Review','') ?>		
								<?php echo input_textarea_group('reviewDetail','Detail',(@$data[0]['reviewDetail']) ? @$data[0]['reviewDetail'] : set_value('reviewDetail'),'Detail ','') ?>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
									&nbsp;
									<a href="<?php echo base_url() ?>index.php/<?php echo getModule() ?>/<?php echo getController() ?>" class="btn btn-inverse"><i class="fa fa-times"></i> Batal</a>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>