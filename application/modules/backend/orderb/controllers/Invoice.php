<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MY_Controller {

	public function index()
	{
		if(hak_akses('view') === FALSE){
			show_errPrivilege();
			exit();
		}
		$data['invoice']   	= $this->model->get('order_payment');
		$data['content'] 	= 'order_invoice_content';
		
		
		$this->load->view('backend/main',$data,FALSE);
	}

	
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */