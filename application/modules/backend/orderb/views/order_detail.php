<style type="text/css">
.custom-table{width: 100%;}
.custom-table tr{height: 0px;font-size: 1.1em}
.custom-table tr th{height: 30px;background: #f2f2f2;padding: 15px 0 15px 5px;}
.custom-table tr  td{padding:5px;vertical-align: top !important}

</style>
<div class="container">
	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title">Order Detail</h4>
				</div>
				<div class="panel-body">
					<div class="row" style="margin-top:20px;">
						<div class="col-md-6 col-sm-12 col-xs-12 " style="padding:15px 30px">
							<h4><b>Order Information</b></h4><hr>
							<table class="custom-table">
								<tr>
									<td >Order Code</td>
									<td><?=$data[0]['orderCode']?></td>
								</tr>
								<tr>
									<td >Cabang</td>
									<td><?=$data[0]['branchName']?></td>
								</tr>
								<tr>
									<td>Name</td>
									<td><?=$data[0]['orderName']?></td>
								</tr>
								<tr>
									<td>Email</td>
									<td>Rp. <?=$data[0]['orderEmail']?></td>
								</tr>
								<tr>
									<td>Contact</td>
									<td><?=$data[0]['orderContact']?></td>
								</tr>
								<tr>
									<td>Address</td>
									<td><?=$data[0]['orderAddress']?></td>
								</tr>
								<tr>
									<td>Date</td>
									<td><?=getDateTime(strtotime($data[0]['createDate']))?></td>
								</tr>	
								<tr>
									<td>Status</td>
									<td><?=$data[0]['orderStatus']?></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6 col-sm-12 col-xs-12 " style="padding:15px 30px">
							<h4><b>Product</b></h4><hr>
							<table class="custom-table">
								<tr>
									<td width="40%">Product </td>
									<td><?=$data[0]['productName']?></td>
								</tr>
								<tr>
									<td>Weight</td>
									<td><?=$data[0]['productWeight']?> Kg.</td>
								</tr>
								<tr>
									<td>Color</td>
									<td><?=$data[0]['productColor']?></td>
								</tr>
								<tr>
									<td>Price</td>
									<td>Rp. <?=konversi_uang($data[0]['productPrice'])?></td>
								</tr>
								<tr>
									<td>Discount</td>
									<td>Rp. <?=konversi_uang($data[0]['productDiscount'])?></td>
								</tr>
							</table>
							
						</div>
						<div class="col-md-qw col-sm-12 col-xs-12 " style="padding:15px 30px">
							<table class="custom-table" style=";border:1px solid #eee">
								<tr>
									<th>No </th>
									<th width="70%">Product</th>
									<th class="text-center" width="25%">Harga</th>
								</tr>
								
								<tbody>
									<tr>
										<td style="height: 10px"></td>
									</tr>
									<tr>
										<td class="p-15">1</td>
										<td class="p-15"> <?=$data[0]['productName']?></td>
										<td  class="text-right"  class="p-15"><?=konversi_uang($data[0]['productPrice'])?></td>
									</tr>
									<tr>
										<td style="height: 10px"></td>
									</tr>
								</tbody>
								<tfoot style="background: #f2f2f2">
									<tr>
										<td class="text-right" colspan="2">Sub Total</td>
										<td  class="text-right" >- Rp.<?=konversi_uang($data[0]['productPrice'])?></td>
									</tr>
									<tr>
										<td class="text-right" colspan="2">Discount Price</td>
										<td  class="text-right" > - Rp.<?=konversi_uang($data[0]['productDiscount'])?></td>
									</tr>
									<tr>
										<td class="text-right" colspan="2">Order Code</td>
										<td  class="text-right" > <?=konversi_uang($data[0]['uniqCode'])?></td>
									</tr>
									<tr>
										<td class="text-right" colspan="2">Total Price</td>
										<td  class="text-right" > Rp.<?=konversi_uang($data[0]['productPrice']- $data[0]['productDiscount']+$data[0]['uniqCode'])?></td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>