<div class="container">

	<?= getBread() ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-border panel-primary">
				<div class="panel-heading">
					<h4 class="panel-title"> Order </h4>
				</div>
				<div class="panel-body">
					<?php if(hak_akses('create') && getController() == 'incoming'){ ?>
					<div class="row">
						<div class="col-md-12">
							<a href="<?php echo base_url().getModule().'/'.getController('').'/report' ?>"><button type="button" class="btn btn-default btn-primary"><i class="fa fa-plus"> </i> Buat Report</button></a>
						</div>
					</div>
					<?php } ?>
					<div class="row" style="margin-top:20px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<table id="datatable" class="table table-custom table-bordered">
								<thead>
									<tr>
										<th class="whiter">No</th>
										<th class="whiter">Kode Order</th>
										<th class="whiter">Nomor HP</th>
										<th class="whiter">Email</th>
										<th class="whiter">Total Harga</th>
										<th class="whiter">Status</th>
										<th class="whiter">Cabang</th>
										<th class="whiter">Action</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 0; foreach ($order as $key => $value) { $no++;?>
									<tr>
										<td><?=$no?></td>
										<td><?=$value['orderCode']?></td>
										<td><?=$value['orderContact']?></td>
										<td><?=$value['orderEmail']?></td>
										<td><?=konversi_uang($value['orderPrice'])?></td>
										<td><?=$value['branchName']?></td>
										<td><?=$value['orderStatus']?></td>
										<td class="text-center">
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/approve/<?php echo $value['orderId'] ?>">
												<button  data-toggle="tooltip" data-title="<?=@$titleApprove?>" data-placement="bottom"   class="<?=@$hiddenApprove?>  btn btn-icon waves-effect waves-light btn-success btn-xs m-b-5" data-attr="<?= $value['orderId'] ?>"><i class="fa fa-check"></i> </button>
											</a>
											<?php } ?>
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/detail/<?php echo $value['orderId'] ?>">
												<button data-toggle="tooltip" data-title="Detail" data-placement="bottom"   class="btn btn-icon waves-effect waves-light btn-inverse btn-xs m-b-5" data-attr="<?= $value['orderId'] ?>"><i class="fa fa-eye"></i> </button>
											</a>
											<?php } ?>
											<?php if(hak_akses('update')){ ?>
											<a href="<?php echo base_url().getModule() ?>/<?php echo getController() ?>/decline/<?php echo $value['orderId'] ?>">
												<button  data-toggle="tooltip" data-title="Decline Order>" data-placement="bottom"  class="<?=@$hiddenApprove?>  btn btn-icon waves-effect waves-light btn-danger btn-xs m-b-5" data-attr="<?= $value['orderId'] ?>"><i class="fa fa-times"></i> </button>
											</a>
											<?php } ?>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('#datatable-button').DataTable( {
			dom: 'Bfrtip',
			buttons: [
			{
				extend:    'copyHtml5',
				text:      '<i class="fa fa-files-o"></i> Copy',
				titleAttr: 'Copy'
			},
			{
				extend:    'csvHtml5',
				text:      '<i class="fa fa-file-text-o"></i> Simpan file Excel',
				titleAttr: 'CSV'
			},
			{
				extend:    'print',
				text:      '<i class="fa fa-print"></i> Print data',
				titleAttr: 'Print'
			},
			{
				extend:    'pdfHtml5',
				text:      '<i class="fa fa-file-pdf-o"></i> Simpan file PDF',
				titleAttr: 'PDF'
			}
			]
		} );
	} );
</script>