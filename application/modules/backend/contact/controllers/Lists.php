<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends MY_Controller {

	public function index()
	{
		$data['data']    = $this->model->get('contact_us','','','createDate DESC');
		$data['content'] = 'list_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function setting($value='')
	{
		$data['data']    = $this->model->get('contact');
		$data['content'] = 'set_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function reply($message='')
	{

		$data['other_js'] = array(
			'backend/plugins/tinymce/tinymce.min.js'
			);
		$data['data']    = $this->model->get_where('contact_us', 'contactUsId=' . $message);
		$data['content'] = 'reply_content';
		$this->load->view('backend/main',$data,FALSE);
	}

	public function post_message($value='')
	{
		$message = $this->input->post();
		// print_r($message);die();
		$this->load->library('mail');
		$send = $this->mail->send($message['from'], $message['contactUsEmail'] , $message['contactUsSubject'], $message['emailMessage']);
		if ($send == 'SUCCESS') {
			redirect(base_url().'contact/lists','refresh');
		}
		else{
			echo "<script> alert('Pesan Gagal Dikirim') </script>";
			redirect(base_url().'contact/lists/reply/'. $message['contactUsId'],'refresh');
		}
		
	}

	public function save()
	{
		$post = $this->input->post();
		if(hak_akses('update') === FALSE){
			show_errPrivilege();
			exit();
		}
		$post['updateDate'] = date('Y-m-d H:i:s');
		$post['updateBy'] = $this->session->userdata('usernameUser');
		$this->db->update('contact',$post);		
		redirect(getModule().'/'.getController());
	}

}