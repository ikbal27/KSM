<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->report();
	}



	public function request($value='')
	{
		$post = $this->input->post();

		$this->report($post['status'],$post['year'],$post['month']);
	}

	public function report($status='', $year='',$month='')
	{
		$data['other_js'] = array(
			'backend/plugins/datatables/dataTables.bootstrap.js',
			'backend/plugins/datatables/dataTables.responsive.min.js',
			'backend/plugins/datatables/responsive.bootstrap.min.js',
			'backend/plugins/datatables/pdfmake.min.js',
			'backend/plugins/datatables/buttons.print.min.js',
			'backend/plugins/datatables/dataTables.buttons.min.js',
			'backend/plugins/datatables/buttons.bootstrap.min.js',
			'backend/plugins/datatables/vfs_fonts.js',
			'backend/plugins/datatables/buttons.html5.min.js',
		);

		if (!$status) {
			$status = '';
		}
		else{
			$status = ' AND `orderStatus` = "'.$status.'" ';
		}

		if (!$month) {
			$month ='';
		}
		else{
			$month = ' AND MONTH(order.createDate) = ' .$month.' ';
		
		}

		if (!$year) {
			$year = date('Y');
		}	

		$param = 'YEAR(order.createDate) = ' . $year . $month . $status;	

		$data['content'] 	= 'report_content';
		$data['order'] = $this->model->join('order','order.*,master_branch.branchName',array(array('table' => 'master_branch' , 'parameter' => 'order.branchId=master_branch.branchId')), $param, 'order.createDate DESC');

		$this->load->view('backend/main',$data,FALSE);
	}
}

/* End of file dashboard.php */
/* Location: ./application/controllers/dashboard.php */
