<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model {
	private $_table1 = "order";

	public function get_data($like='')
	{
		$this->db->select('*');
		$this->db->from($this->_table1);
		$this->db->like($like);
		return $this->db->get()->result_array();
	}
	

}

/* End of file report_model.php */
/* Location: ./application/models/report_model.php */