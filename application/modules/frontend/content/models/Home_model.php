<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	function getSlider()
	{
		$this->db->where('sliderStatus','t');
		return $this->db->get('slider')->result();
	}

	function getContact()
	{
		return $this->db->get('contact')->result();
	}

	function getKeunggulan()
	{
		return $this->db->get('advantages')->result();
	}

	function getReview()
	{
		return $this->db->get('review')->result();
	}

	function getBranch()
	{
		return $this->db->get('master_branch')->result();
	}

	function getProduct($productId='')
	{
		if ($productId) {
			$this->db->where('productId', $productId);
		}
		return $this->db->get('product')->result();
	}

	function getDriver()
	{
		return $this->db->get('driver')->result();
	}

	function getTestimoni()
	{
		return $this->db->get('testimonials')->result();
	}

	function getSocmed()
	{
		return $this->db->get('socmed')->result();
	}

	function post_message($data='')
	{
		$this->db->insert('contact_us', $data);
	}

	function get_dropdown_product(){

		$getData = $this->getProduct();

		$opt = array();
		foreach ($getData as $key => $listDriver) {
			$opt[$listDriver->productId] = $listDriver->productName;
		}
		return $opt;
	}

	

}

/* End of file Model_content.php */
/* Location: ./application/modules/frontend/content/models/Model_content.php */