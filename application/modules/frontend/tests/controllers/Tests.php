<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tests extends CI_Controller {

	public function index()
	{
		$data['header']  = "frontend/layout/header_scroll";
		$data['content'] = "test_content";
		$this->load->view('frontend/layout/main', $data, FALSE);
	}


}

/* End of file Bulletin.php */
/* Location: ./application/modules/frontend/bulletin/controllers/Bulletin.php */