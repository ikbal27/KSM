		<section>
			<div class="slider-area">
				<div class="home-slider">
					<?php foreach ($product as $key => $value) {  ?>
					<div class="pt-100 slider-item bg-img slide-bg-1 sm-no-padding">
						<div class="container">
							<div class="row flexbox-center">
								<div class="col-md-5 hidden-xs hidden-sm">
									<div class="p-30 text-center wow zoomIn optin-form">
										<img src="<?=base_url().'/assets/uploads/product/'.$value->productImage?>">
									</div>
								</div>
								<div class="col-md-offset-1 col-md-6">
									<div class="banner-text sm-text-center wow fadeInDown">
										<h1><?=$value->productName?></h1>
										<p style="font-size: 1.3em;margin-top: -30px;"><?=$value->overviewCaption?></p>
										<?php if ($value->productDiscount) { ?>										
										<p style="font-size: 1.4em;margin-top: -40px;margin-bottom: 0">Harga Rp : <b><?=konversi_uang($value->productPrice-$value->productDiscount)?> </b><small style="font-size: 0.8em">Rp. <s><?=konversi_uang($value->productPrice)?></s></small></p>
										<p style="font-size: 1em;">Discount Rp : <?=konversi_uang($value->productDiscount)?></p>
										<?php }else{ ?>
										<p style="font-size: 1.5em;margin-top: -40px;margin-bottom: 30">Harga Rp : <?=konversi_uang($value->productPrice)?></p>
										<?php } ?>
										<div class="btn-set" style="margin-top: -15px;">
											<a id="download_link" href="<?=base_url().'order/'.$value->productSlug?>" style="font-size: 1.1em" class="btn btn-round btn-transparent <?=$value->stockButton?>">&nbsp; &nbsp; &nbsp; Beli Produk <i class="icofont icofont-cart-alt"></i> </a>
											<a id="download_link" href="#" style="font-size: 1.1em" class="btn btn-round btn-transparent <?=$value->emptyButton?>">&nbsp; &nbsp; &nbsp; Produk Habis <i class="icofont icofont-cart-alt"></i> </a>
											<a id="download_link" href="<?=$value->productDriver?>" style="font-size: 1.1em" class="btn btn-round btn-transparent">&nbsp; &nbsp; &nbsp; Download Driver <i class="icofont icofont-cloud-download"></i> </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>
	</header>

	

	<section>
		<div class="padding-big bg-white">
			<div class="container">
				<h2><b>Spesifikasi produk <?=$value->productName?></b></h2><br>
				<table width="100%" style="border:1px solid #c4c4c4;border-radius: 3px" >
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Nama</td>
						<td style="text-indent: 30px"><?=$value->productName?></td>
					</tr>
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Berat</td>
						<td style="text-indent: 30px"><?=$value->productWeight?> Kg.</td>
					</tr>
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Warna</td>
						<td style="text-indent: 30px"><?=$value->productColor?>.</td>
					</tr>
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Harga</td>
						<td style="text-indent: 30px">Rp <?=konversi_uang($value->productPrice)?> </td>
					</tr>
					<?php if ($value->productDiscount) { ?>										
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Discount</td>
						<td style="text-indent: 30px">Rp.  <?=konversi_uang($value->productDiscount)?></td>
					</tr>
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Total Harga</td>
						<td style="text-indent: 30px">Rp.  <?=konversi_uang($value->productPrice-$value->productDiscount)?></td>
					</tr>
					<?php } ?>
					
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Stok</td>
						<td style="text-indent: 30px"><?=$value->productStock?> Buah.</td>
					</tr>
					<tr style="border-bottom: 1px solid #c4c4c4">
						<td style="text-indent:10px;padding: 7px;border-right: 1px solid #c4c4c4" class="light-bg" width="25%"> Driver</td>
						<td style="text-indent: 30px"><a href="<?=$value->productDriver?>">Click to Download</a></td>
					</tr>
				</table>
				<br>
				<h2><b>Detail produk <?=$value->productName?></b></h2><br>
				<?=$value->productDescription?>

				<div class="text-center">
					<a id="download_link" href="<?=base_url().'order/'.$value->productSlug?>" style="font-size: 1.1em" class="btn btn-round btn-red btn-submit <?=$value->stockButton?>">&nbsp; &nbsp; &nbsp;<i class="icofont icofont-cart-alt"></i> Beli Produk  </a>
					<a id="download_link" href="#" style="font-size: 1.1em" class="btn btn-round btn-red btn-submit <?=$value->emptyButton?>">&nbsp; &nbsp; &nbsp;<i class="icofont icofont-cart-alt"></i> Produk Habis </a>
					<a id="download_link" href="<?=$value->productDriver?>" style="font-size: 1.1em" class="btn btn-round btn-red btn-submit">&nbsp; &nbsp; &nbsp; <i class="icofont icofont-cloud-download"></i> Download Driver </a>
					<a id="download_link" href="<?=base_url().'product'?>" style="font-size: 1.1em" class="btn btn-round btn-red btn-submit">&nbsp; &nbsp; &nbsp; <i class="icofont icofont-justify-all"></i> List Produk </a>

				</div>
			</div>
		</div>
	</section>

	<div id="back-top">
		<a href="#top"><i class="icofont icofont-arrow-up circled-icon"></i></a>
	</div>


	<script type="text/javascript">
		$('#multiple-items').slick({
			infinite: true,
			slidesToShow: 4,
			slidesToScroll: 2,
			autoplay: true,
			autoplaySpeed: 1500,
			arrows:true,
		});
	</script>