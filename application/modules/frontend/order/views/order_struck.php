<div class="row" style="margin: 0">
	<div class="col-xs-12" style="padding: 30px">
		<div class="row" style="margin:0;border: 1px solid #c4c4c4;padding: 15px;">
			<div class="col-xs-6">
				<img src="<?=base_url().'assets/img/logo/logo.png'?>" width="144px;">
			</div>
			<div class="col-xs-6" style="text-align: right;margin-top: 15px;">
				<H4> <?=$struck[0]->orderCode?> </H4>
			</div>
			<div class="col-xs-12" style="margin: 15px 0">
				<div class="col-xs-6">
					<div style="font-weight: bold;font-size: 1.4em">Billing Information</div>
					<hr style="margin-top: -0px">
					<p>
						Name : <?=$struck[0]->orderName?> <br>
						Contact : <?=$struck[0]->orderContact?> <br>
						Email : <?=$struck[0]->orderEmail?> <br>
						Address : <?=$struck[0]->orderAddress?> <br>
						Order Date : <?=getDateTime(strtotime($struck[0]->createDate))?> <br>
						Status : <?=$struck[0]->orderStatus?> <br>
					</p>
				</div>

				<div class="col-xs-6">
					<div style="font-weight: bold;font-size: 1.4em">Product Information</div>
					<hr style="margin-top: -0px">
					<p>
						Product : <?=$struck[0]->productName?> <br>
						Weight : <?=$struck[0]->productWeight?> Kg.<br>
						Color : <?=$struck[0]->productColor?> <br>
						Price : <?=konversi_uang($struck[0]->productPrice)?> <br>
						Discount : <?=konversi_uang($struck[0]->productDiscount)?> <br>
					</p>
				</div>
			</div>
			<div class="col-xs-12">
				<div style="font-weight: bold;font-size: 1.4em">Item</div>
				<table class="table table-hovered">
					<thead>
						<tr>
							<td>No</td>
							<td width="70%">Product</td>
							<td class="text-center" width="25%" st>Price</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1<br><br></td>
							<td> <?=$struck[0]->productName?></td>
							<td class="text-right"><?=konversi_uang($struck[0]->productPrice)?></td>
						</tr>
						<tr>
					</tbody>
					<tfoot >
						<tr class="light-bg">
							<td class="text-right" colspan="2">Sub Total</td>
							<td class="text-right">Rp <?=konversi_uang($struck[0]->productPrice)?></td>
						</tr>
						<tr class="light-bg">
							<td class="text-right" colspan="2">Discount Price</td>
							<td class="text-right"> - Rp <?=konversi_uang($struck[0]->productDiscount)?></td>
						</tr>
						<tr class="light-bg">
							<td class="text-right" colspan="2">Order Code</td>
							<td class="text-right"> <?=konversi_uang($struck[0]->uniqCode)?></td>
						</tr>
						<tr class="light-bg">
							<td class="text-right" colspan="2">Total Price</td>
							<td class="text-right">Rp <?=konversi_uang($struck[0]->productPrice- $struck[0]->productDiscount+$struck[0]->uniqCode)?></td>
						</tr>
						<tr>
							<td class="text-right" colspan="2"></td>
							<td class="text-right"><small style="color: red"><b>Please Transfer 3 digits !</b></small></td>
						</tr>
					</tfoot>
				</table>
				<center>
					<br><span class=" text-muted" style="font-size: 0.8em;font-style: italic;font-weight: bold" ><?=getDateTime(strtotime(date('Y-m-d h:i:s')))?></span>
				</center>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	window.print();
	window.open('<?=base_url()?>','_blank');
</script>