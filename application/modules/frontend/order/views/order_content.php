		<section>
			<div id="home" class="slider-area">
				<div class="home-slider">
					
					<div class="pt-50 slider-item bg-img slide-bg-1 sm-no-padding" style="height: 90px">
						<div class="container">
							<div class="row flexbox-center">
								<div class="col-md-4 hidden-xs hidden-sm">
									<div class="p-30 text-center wow zoomIn optin-form">
										
									</div>
								</div>
								<div class="col-md-offset-1 col-md-6">
									<div class="banner-text sm-text-center wow fadeInDown">
										
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</section>
	</header>

	<section>
		<div class="container" >
			<div class="row " style="padding: 100px 0;z-index: 9">
				<div class="">
					<div class="hidden-md hidden-lg" id="cart"  style="padding: 30px">
						<div style="padding: 15px 0">
							<section style="border: 1px solid #c4c4c4;">
								<div style="padding:15px 30px">
									
									<h6 class="light-bg" style="font-size: 1.3em;border-bottom: 1px solid #c4c4c4;margin: -15px -30px;padding: 10px; ">
										Rincian	 Pesanan
									</h6>
									<div class="img-wrap">
										<center>
											<img src="<?=base_url().'assets/uploads/product/'. $product[0]->productImage?>" class="img-responsive"  >
										</center>
									</div>
									Nama Barang   : <?=$product[0]->productName?><br>
									Berat   : <?=$product[0]->productWeight?><br>
									Warna   : <?=$product[0]->productColor?><br>
									
								</div>
								<div style="padding:0 10px;font-size: 1.1em;border-top: 1px solid #c4c4c4" class="light-bg">
									<font size="3.5em">Harga</font>
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productPrice)?>
									</div>
									<font size="3.5em">Diskon</font>
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productDiscount)?>
									</div>
									<font size="3.5em">Total Harga</font>  
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productPrice-$product[0]->productDiscount)?>
									</small>
								</div>
								<small style="top: -10px"><font size="1" style="font-style: italic;font-weight: bold;color: #ec3851" >** Harga Belum termasuk Ongkir</font>
								</div>
							</section>
						</div>
					</div>

					<div class="col-md-8" >
						<div align="">
							<h2>
							Order Form </h2> <h4 style="margin-top: 0px;font-size: 1em">Harap lengkapi form di bawah untuk memproses pesanan.</h4>
							
							<hr style="margin-top: -0px">
						</div>
						<form action="<?=base_url().getModule().'/post'?>" method="post">
							<input type="hidden" name="productId" value="<?=$product[0]->productId?>">
							<input type="hidden" name="orderPrice" value="<?=$product[0]->productPrice-$product[0]->productDiscount?>">
							<div class="form-group">
								<label for="exampleInputEmail1">Nama </label>
								<input name="orderName" class="form-control" id="exampleInputEmail1" placeholder="Nama" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Email</label>
								<input name="orderEmail" type="email" class="form-control" id="exampleInputEmail1" placeholder="Email" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Nomor Hp.</label>
								<input name="orderContact" type="number" class="form-control" id="exampleInputEmail1" placeholder="No. Hp" required="">
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Cabang Terdekat</label>
								<select class='form-control' data-placeholder='Pilih Device' name='branchId' id='device' required="" >
									<option selected="" disabled="">Pilih Cabang</option>
									<?php foreach ($branch as  $optValue) { ?>
									<option value="<?=$optValue->branchId?>"><?=$optValue->branchName?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail1">Alamat Pengiriman</label>
								<textarea  required="" name="orderAddress" type="text" class="form-control" id="exampleInputEmail1" placeholder="Alamat pengiriman"></textarea>
							</div>
							<div class="form-group text-center" style="">
								<button id="send_message" style="padding: " class="btn btn-round btn-red btn-submit">Order</button>
								<a href="<?=base_url()?>" class="btn btn-round btn-white btn-submit" style="border: 1px solid #ec3851;padding: ">Cancel</a>
							</div>
						</form>

					</div>

					<div class="col-md-4 hidden-sm hidden-xs" id="cart"  style="">
						<div style="padding: 15px 0">
							<section style="border: 1px solid #c4c4c4;">
								<div style="padding:15px 30px">
									
									<h6 class="light-bg" style="font-size: 1.3em;border-bottom: 1px solid #c4c4c4;margin: -15px -30px;padding: 10px; ">
										Rincian	 Pesanan
									</h6>
									<div class="img-wrap">
										<center>
											<img src="<?=base_url().'assets/uploads/product/'. $product[0]->productImage?>" class="img-responsive"  >
										</center>
									</div>
									Nama Barang   : <?=$product[0]->productName?><br>
									Berat   : <?=$product[0]->productWeight?><br>
									Warna   : <?=$product[0]->productColor?><br>
									
								</div>
								<div style="padding:0 10px;font-size: 1.1em;border-top: 1px solid #c4c4c4" class="light-bg">
									<font size="3.5em">Harga</font>
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productPrice)?>
									</div>
									<font size="3.5em">Diskon</font>
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productDiscount)?>
									</div>
									<font size="3.5em">Total Harga</font>  
									<div align="right" style="margin-top: -30px">
										Rp <?=konversi_uang($product[0]->productPrice-$product[0]->productDiscount)?>
									</small>
								</div>
								<small style="top: -10px"><font size="1" style="font-style: italic;font-weight: bold;color: #ec3851" >** Harga Belum termasuk Ongkir</font>
								</div>
							</section>
						</div>
					</div>

				</div>
			</div>
		</div>
	</section>